package br.unibh.sdm.appcriptomoeda2.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import br.unibh.sdm.appcriptomoeda2.R;
import br.unibh.sdm.appcriptomoeda2.api.RestServiceGenerator;
import br.unibh.sdm.appcriptomoeda2.api.UserService;
import br.unibh.sdm.appcriptomoeda2.entidades.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormularioUserActivity extends AppCompatActivity {

    private UserService service = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_usuarios);
        setTitle("Edição de User");
        service = RestServiceGenerator.createService(UserService.class);
        configuraBotaoSalvar();
        inicializaObjeto();
    }

    private void inicializaObjeto() {
        Intent intent = getIntent();
        if (intent.getSerializableExtra("objeto") != null) {
            User objeto = (User) intent.getSerializableExtra("objeto");
            EditText name = findViewById(R.id.editTextName);
            EditText cpf = findViewById(R.id.editTextCpf);
            EditText bio = findViewById(R.id.editTextTextMultiLineBio);
            name.setText(objeto.getFirstName());
            cpf.setText(objeto.getCpf());
            bio.setText(objeto.getBio());
            name.setEnabled(false);
            Button botaoSalvar = findViewById(R.id.buttonSalvar);
            botaoSalvar.setText("Atualizar");
        }
    }

    private void configuraBotaoSalvar() {
        Button botaoSalvar = findViewById(R.id.buttonSalvar);
        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("FormularioUser","Clicou em Salvar");
                User user = recuperaInformacoesFormulario();
                Intent intent = getIntent();
                if (intent.getSerializableExtra("objeto") != null) {
                    User objeto = (User) intent.getSerializableExtra("objeto");
                    user.setCpf(objeto.getCpf());
                    if (validaFormulario(user)) {
                        atualizaUsuario(user);
                    }
                } else {
                    if (validaFormulario(user)) {
                        salvaUsuario(user);
                    }
                }
            }
        });
    }

    private boolean validaFormulario(User user){
        boolean valido = true;
        EditText name = findViewById(R.id.editTextName);
        EditText cpf = findViewById(R.id.editTextCpf);
        EditText bio = findViewById(R.id.editTextTextMultiLineBio);
        if (user.getFirstName() == null || user.getFirstName().trim().length() == 0){
            name.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
            valido = false;
        } else {
            name.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_blue_dark), PorterDuff.Mode.SRC_ATOP);
        }
        if (user.getCpf() == null || user.getCpf().trim().length() == 0){
            cpf.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
            valido = false;
        } else {
            cpf.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_blue_dark), PorterDuff.Mode.SRC_ATOP);
        }
        if (user.getBio() == null || user.getBio().trim().length() == 0){
            bio.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
            valido = false;
        } else {
            bio.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_blue_dark), PorterDuff.Mode.SRC_ATOP);
        }
        if (!valido){
            Log.e("FormularioUser", "Favor verificar os campos destacados");
            Toast.makeText(getApplicationContext(), "Favor verificar os campos destacados", Toast.LENGTH_LONG).show();
        }
        return valido;
    }

    private void salvaUsuario(User user) {
        Call<User> call;
        Log.i("FormularioUser","Vai criar user "+ user.getCpf());
        call = service.createUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Log.i("FormularioUser", "Salvou a User " + user.getCpf());
                    Toast.makeText(getApplicationContext(), "Salvou a User " + user.getCpf(), Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Log.e("FormularioUser", "Erro (" + response.code()+"): Verifique novamente os valores");
                    Toast.makeText(getApplicationContext(), "Erro (" + response.code()+"): Verifique novamente os valores", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("FormularioUser", "Erro: " + t.getMessage());
            }
        });
    }

    private void atualizaUsuario(User user) {
        Call<User> call;
        Log.i("FormularioUser","Vai atualizar user "+ user.getCpf());
        call = service.updateUser(user.getCpf(), user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Log.i("FormularioUser", "Atualizou a User " + user.getCpf());
                    Toast.makeText(getApplicationContext(), "Atualizou a User " + user.getCpf(), Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Log.e("FormularioUser", "Erro (" + response.code()+"): Verifique novamente os valores");
                    Toast.makeText(getApplicationContext(), "Erro (" + response.code()+"): Verifique novamente os valores", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("FormularioUser", "Erro: " + t.getMessage());
            }
        });
    }

    @NotNull
    private User recuperaInformacoesFormulario() {
        EditText name = findViewById(R.id.editTextName);
        EditText cpf = findViewById(R.id.editTextCpf);
        EditText bio = findViewById(R.id.editTextTextMultiLineBio);
        User user = new User();
        user.setFirstName(name.getText().toString());
        user.setCpf(cpf.getText().toString());
        user.setBio(bio.getText().toString());
        return user;
    }
}