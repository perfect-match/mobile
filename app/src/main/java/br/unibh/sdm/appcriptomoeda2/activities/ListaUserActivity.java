package br.unibh.sdm.appcriptomoeda2.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import br.unibh.sdm.appcriptomoeda2.R;
import br.unibh.sdm.appcriptomoeda2.api.RestServiceGenerator;
import br.unibh.sdm.appcriptomoeda2.api.UserService;
import br.unibh.sdm.appcriptomoeda2.entidades.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaUserActivity extends AppCompatActivity {

    private UserService service = null;
    private final ListaUserActivity listaUserActivity = this;
    private final Context context;

    public ListaUserActivity() {
        context = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Lista de Perfect Users");
        setContentView(R.layout.activity_lista_usuarios);
        service = RestServiceGenerator.createService(UserService.class);
        criaAcaoBotaoFlutuante();
        criaAcaoCliqueLongo();
    }

    private void criaAcaoCliqueLongo() {
        ListView listView = findViewById(R.id.listViewListaUsuarios);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("ListaUserActivity","Clicou em clique longo na posicao "+position);
                final User objetoSelecionado = (User) parent.getAdapter().getItem(position);
                Log.i("ListaUserActivity", "Selecionou o user "+objetoSelecionado.getCpf());
                new AlertDialog.Builder(parent.getContext()).setTitle("Removendo User")
                        .setMessage("Tem certeza que quer remover o user "+objetoSelecionado.getCpf()+"?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                removeCriptomoeda(objetoSelecionado);
                            }
                        }).setNegativeButton("Não", null).show();
                return true;
            }
        });
    }

    private void removeCriptomoeda(User user) {
        Call<Boolean> call = null;
        Log.i("ListaUserActivity","Vai remover user com o cpf "+ user.getCpf());
        call = this.service.deleteUser(user.getCpf());
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Log.i("ListaUserActivity", "Removeu o User com o cpf " + user.getCpf());
                    Toast.makeText(getApplicationContext(), "Removeu o User com o cpf " + user.getCpf(), Toast.LENGTH_LONG).show();
                    onResume();
                } else {
                    Log.e("ListaUserActivity", "Erro (" + response.code()+"): Verifique novamente os valores");
                    Toast.makeText(getApplicationContext(), "Erro (" + response.code()+"): Verifique novamente os valores", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("ListaUserActivity", "Erro: " + t.getMessage());
            }
        });
    }

    private void criaAcaoBotaoFlutuante() {
        FloatingActionButton botaoNovo = findViewById(R.id.floatingActionButtonCriar);
        botaoNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("ListaUserActivity","Clicou no botão para adicionar novo User");
                startActivity(new Intent(ListaUserActivity.this,
                        FormularioUserActivity.class));

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        buscaUsuarios();
    }

    public void buscaUsuarios(){
        Call<List<User>> call = this.service.getUsers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    Log.i("ListaUserActivity", "Retornou " + response.body().size() + " Pessoas!");
                    ListView listView = findViewById(R.id.listViewListaUsuarios);
                    listView.setAdapter(new ListaUserAdapter(context,response.body()));
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Log.i("ListaUserActivity", "Selecionou o objeto de posicao "+position);
                            User objetoSelecionado = (User) parent.getAdapter().getItem(position);
                            Log.i("ListaUserActivity", "Selecionou "+objetoSelecionado.getFirstName());
                            Intent intent = new Intent(ListaUserActivity.this, FormularioUserActivity.class);
                            intent.putExtra("objeto", objetoSelecionado);
                            startActivity(intent);
                        }
                    });
                } else {
                    Log.e("ListaUserActivity", "" + response.message());
                    Toast.makeText(getApplicationContext(), "Erro (" + response.code()+"): "+ response.message(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("ListaUserActivity", "Erro: " + t.getMessage());
            }
        });
    }
}