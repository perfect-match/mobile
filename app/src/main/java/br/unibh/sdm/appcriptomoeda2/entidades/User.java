package br.unibh.sdm.appcriptomoeda2.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class User implements Serializable {

    private String firstName;
    private String lastName;
    private String nickname;
    private String email;
    private String address;
    private String bio;
    private String cpf;
    private String rg;
    private Date birthDate;
    private Boolean isPremium;

    public User() {}

    public User(String firstName, String lastName, String nickname, String email, String address, String bio, String cpf, String rg, Date birthDate, Boolean isPremium) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickname = nickname;
        this.email = email;
        this.address = address;
        this.bio = bio;
        this.cpf = cpf;
        this.rg = rg;
        this.birthDate = birthDate;
        this.isPremium = isPremium;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Boolean getPremium() {
        return isPremium;
    }

    public void setPremium(Boolean premium) {
        isPremium = premium;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", bio='" + bio + '\'' +
                ", cpf='" + cpf + '\'' +
                ", rg='" + rg + '\'' +
                ", birthDate=" + birthDate +
                ", isPremium=" + isPremium +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getFirstName(), user.getFirstName()) &&
                Objects.equals(getLastName(), user.getLastName()) &&
                Objects.equals(getNickname(), user.getNickname()) &&
                Objects.equals(getEmail(), user.getEmail()) &&
                Objects.equals(getAddress(), user.getAddress()) &&
                Objects.equals(getBio(), user.getBio()) &&
                Objects.equals(getCpf(), user.getCpf()) &&
                Objects.equals(getRg(), user.getRg()) &&
                Objects.equals(getBirthDate(), user.getBirthDate()) &&
                Objects.equals(isPremium, user.isPremium);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getNickname(), getEmail(), getAddress(), getBio(), getCpf(), getRg(), getBirthDate(), isPremium);
    }
}
