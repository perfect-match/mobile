package br.unibh.sdm.appcriptomoeda2.api;

import java.util.List;

import br.unibh.sdm.appcriptomoeda2.entidades.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @Headers({
            "Accept: application/json",
            "User-Agent: AppPerfectMatchUsers"
    })
    @GET("users")
    Call<List<User>> getUsers();

    @GET("users/{cpf}")
    Call<User> getUserByCpf(@Path("cpf") String cpf);

    @POST("users")
    Call<User> createUser(@Body User user);

    @PUT("users/{cpf}")
    Call<User> updateUser(@Path("cpf") String cpf, @Body User user);

    @DELETE("users/{cpf}")
    Call<Boolean> deleteUser(@Path("cpf") String cpf);

}
